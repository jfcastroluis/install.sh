#!/usr/bin/env bash

set -e

INSTALLSH_DEPENDENCIES=(awk curl install jq mktemp tr sed uname unzip)
INSTALLSH_DEST="${INSTALLSH_DEST:-${HOME}/.local/bin}"
INSTALLSH_TMP="${INSTALLSH_TMP:-/tmp}"
INSTALLSH_COMMAND=${INSTALLSH_COMMAND:-install}

APP_NAME=terraform
APP_REPO_USER=hashicorp
APP_REPO_NAME=${APP_NAME}
APP_REPO_URL=https://github.com/${APP_REPO_USER}/${APP_REPO_NAME}
APP_REPO_API=https://api.github.com/repos/${APP_REPO_USER}/${APP_REPO_NAME}

APP_VERSION=${APP_VERSION:-$(curl -s ${APP_REPO_API}/releases/latest | jq -r .tag_name | awk '{print substr($1, 2)}')}

APP_OS="${APP_OS:-$(uname | tr '[:upper:]' '[:lower:]')}"
APP_ARCH="${APP_ARCH:-$(uname -m | tr '[:upper:]' '[:lower:]' | sed -e s/x86_64/amd64/)}"

APP_FILE_DIR=$(mktemp -d -p ${INSTALLSH_TMP} installsh.XXX)
APP_FILE_EXTENSION="zip"
APP_FILE_PATH=${APP_FILE_DIR}/${APP_NAME}-${APP_VERSION}-${APP_OS}-${APP_ARCH}.${APP_FILE_EXTENSION}
APP_FILE_URL="https://releases.hashicorp.com/${APP_NAME}/${APP_VERSION}/${APP_NAME}_${APP_VERSION}_${APP_OS}_${APP_ARCH}.${APP_FILE_EXTENSION}"
APP_FILE_BINARY=${APP_NAME}

function check_dependencies() {
  echo "[*] Checking dependencies are installed"
  for dep in "${INSTALLSH_DEPENDENCIES[@]}"; do
    echo -ne "\t${dep}...\t"
    if [ $(command -v ${dep}) ]; then
      echo ok
    else
      echo "not found"
      exit 1
    fi
  done
}

function download() {
  echo "[*] Downloading file"
  echo -ne "\t$1 to $2... "
  curl -sL "$1" -o "$2"
  echo "done"
}

function uncompress() {
  echo "[*] Uncompressing file"
  echo -ne "\t$1 to $2... "
  #tar -C $2 -xzf $1
  unzip $1 -d $2 > /dev/null
  echo "done"
}

function install() {
 echo "[*] Installing file"
 echo -ne "\t$1 in $2... "
 command install $1 $2
 echo "done"
}

function symlink() {
  echo "[*] Creating symlink"
  echo -ne "\tfrom $1 to $2... "
  ln -sf $1 $2
  echo "done"
}

function clean() {
  echo "[*] Removing directory"
  echo -ne "\t$1... "
  rm -rf $1
  echo "done"
}

function removebinary {
  rm -rf $1
}

function removesymlink {
  find $1 -xtype l -execdir rm {} \;
}


check_dependencies

case ${INSTALLSH_COMMAND} in
  "install")
    tput civis
    download ${APP_FILE_URL} ${APP_FILE_PATH}
    uncompress ${APP_FILE_PATH} ${APP_FILE_DIR}
    install ${APP_FILE_DIR}/${APP_FILE_BINARY} ${INSTALLSH_DEST}/${APP_NAME}-${APP_VERSION}
    symlink ${APP_NAME}-${APP_VERSION} ${INSTALLSH_DEST}/${APP_NAME}
    clean ${APP_FILE_DIR}
    tput cnorm
    ;;
  "uninstall")
    removebinary ${INSTALLSH_DEST}/${APP_NAME}-${APP_VERSION}
    removesymlink ${INSTALLSH_DEST}/${APP_NAME}
    clean ${APP_FILE_DIR}
    ;;
  *)
    exit 1
esac


exit 0
