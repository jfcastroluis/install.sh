# install.sh

## Pasos para un script de instalacion

1. Detección de ultima version
2. Descarga del artefacto de la aplicación
3. Tratamiento del artefacto
    - binario: nada
    - tarball: descomprimir
4. Instalacion del binario
    - permisos
    - link
5. Limpieza


## Posibles mejoras

- Borrado de versiones antiguas
- Posibilidad de instalar las dependencias
- Comprobacion de checksum
- Modo verbose con echo

## Notas
Ver el instalador de Kitty https://sw.kovidgoyal.net/kitty/binary/#customizing-the-installation
